const sortArrayObject = async (array, field) => {
    return await array.sort((a,b) => {
        return a[field].toLowerCase().localeCompare(b[field].toLowerCase());
    });
}


export { sortArrayObject };