const transformMessage = (request, levelLog, error) => {

    const message = {
        requestHost: request.headers.host,
        requestMethod: request.method,
        requestUrl: request.url,
        requestDate: new Date(),
        requestBody: request.body,
        message: levelLog == 'info' ? 'Requisição aceita com sucesso' : `Erro ao processar a requisição, ${error}`
    }

    return message;
}


export default transformMessage;