import getUsers from '../services/userGetServices';
import { sortArrayObject } from '../utils/arraySortAlphabetically';

const url = 'https://jsonplaceholder.typicode.com/users';


const responseGetUser = async() => {
    return await getUsers(url);
}

// Lista all User websites
const listAllWebsites = async() => {
    try {
        const result = await responseGetUser();
        const websites = await result.map(e => e.website);
        //Return all websites.
        return websites;

    } catch (error) {
        return error;
    }
}

const listAllUsersAlphabeticOrder = async() => {
    try {
        const result = await responseGetUser();
        //Return all user name, email and company.
        const userObject = await result.map(e => { return { name: e.name, email: e.email, company: e.company} });
        //Use helper sortAlphabetically to sort userObject based on name.
        const userObjectAlphabeticallySort = await sortArrayObject(userObject, 'name');
        //Return userObject sorted.
        return userObjectAlphabeticallySort;
    } catch (error) {
        return error;
    }
};

const listAllUsersWithSuite = async(req, res) => {
    try {
        const result = await responseGetUser();
        //Filter all users that have Suite in Suite address.
        const userObject = await result.filter(e => e.address.suite.indexOf('Suite') !== -1);
        //Return userObject filtered.
        return userObject;
    } catch (error) {
        return error;
    }
}

export { listAllWebsites, listAllUsersAlphabeticOrder, listAllUsersWithSuite };