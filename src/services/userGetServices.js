import axios from 'axios';

const getUsers = async (url) => {
    const response = await axios.get(url);
    return response.data;
}

export default getUsers;