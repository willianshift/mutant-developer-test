import express from 'express';

import { listAllUsersAlphabeticOrderController, listAllWebsitesController, listAllUsersWithSuiteController  } from '../controllers/userController';

const router = express.Router();

const userRoutes = router.get('/websites', listAllWebsitesController)
                   router.get('/suites', listAllUsersWithSuiteController )
                   router.get('/alphabeticOrder', listAllUsersAlphabeticOrderController)


export default userRoutes;