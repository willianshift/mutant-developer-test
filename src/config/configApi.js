import express from 'express';
const app = express();

const startApp = () => {
    app.listen(process.env.API_PORT || 8080, () => {
        console.log('working on port 8080');
    })
}

export { startApp, app };