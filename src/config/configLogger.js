import winston from 'winston';
const logger = winston.createLogger();

import elasticSearch from 'elasticsearch';
import winstonElasticSearch from 'winston-elasticsearch';

import messageHelper from '../utils/messageHelper';

const client = new elasticSearch.Client({
    host: process.env.ELASTICSEARCH_URL || 'localhost:9200',
    log: 'info',
});

logger.add(new winstonElasticSearch({
    client, 
    index: 'logapi_requests'
}));

const loggers = (request, levelLog, error) => {
    return logger.log(levelLog, messageHelper(request, levelLog, error));
}

export default loggers;
