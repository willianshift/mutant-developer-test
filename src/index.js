import loggers from './config/configLogger';
import { startApp, app } from './config/configApi';

import userRoutes from './routes/userRoutes';

app.get('/', async (req, res) => {
    loggers(req, 'info');

    res.send({
        api: 'v1',
        success: true
    });
})

app.use('/users/', userRoutes);

startApp();

export default app;