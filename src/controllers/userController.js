import { listAllWebsites, listAllUsersAlphabeticOrder, listAllUsersWithSuite } from '../services/userListServices';
import logger  from '../config/configLogger';

// Lista all User websites
const listAllWebsitesController = async(req, res) => {
    try {
        //Get all websites on service listAll;
        const websites = await listAllWebsites();
        logger(req, 'info', websites);
        res.status(200).send({
            success: true,
            result: websites
        });
    } catch (error) {
        logger(req, 'error', error);
        res.status(500).send({
            success: false,
            req,
            error
        });
    }
}

// List all users, return name, email and company after all, sort Alphabetically;
const listAllUsersAlphabeticOrderController = async(req, res) => {
    try {
        //Get all users on alphabetically order on service listAll;
        const userObjectAlphabeticallySort = await listAllUsersAlphabeticOrder();
        //Logger to elasticSearch
        logger(req, 'info', userObjectAlphabeticallySort);
        //Send res to client.
        res.status(200).send({
            success: true,
            result: userObjectAlphabeticallySort
        });
    } catch (error) {
        logger(req, 'error', error);
        res.status(500).send({
            success: false,
            req,
            error
        });
    }
};

// List All Users with Suite in Suite. 
const listAllUsersWithSuiteController = async(req, res) => {
    try {
        const userObject = await listAllUsersWithSuite();

        logger(req, 'info', userObject);
        //Send res to client.
        res.status(200).send({
            success: true,
            result: userObject
        });
        
    } catch (error) {
        logger(req, 'error', error);
        res.status(500).send({
            success: false,
            req,
            error
        });
    }
}

export { listAllWebsitesController, listAllUsersWithSuiteController , listAllUsersAlphabeticOrderController };