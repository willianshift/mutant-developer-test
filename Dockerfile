FROM keymetrics/pm2:latest-alpine 

COPY src src/
COPY package.json .
COPY pm2.json .
COPY yarn.lock .
COPY webpack.config.js .
COPY tests tests/
COPY .babelrc .

RUN ls src/

RUN npm install -g yarn

RUN yarn install

RUN yarn build

RUN echo "Build finalizado "

CMD [ "pm2-runtime", "start", "pm2.json" ]