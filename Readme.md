# Test Mutant developer.

Repositório do teste para desenvolvedor na Mutant.

## Como Inicializar o projeto:

 Instalar o plugin Vagrant Docker Compose:
 

    vagrant plugin install vagrant-docker-compose

Iniciar o projeto com o vagrant: 
	

    vagrant up
O build era executar, criar os containers com Docker, apos alguns segundos o elasticsearch irá subir, para verificar se o mesmo está "up" acessar:

    http://localhost:9200

Após o elasticSearch subir, a API já estará logando toda requisição feita nos endpoints abaixo:

 1. Os websites de todos os usuário
	 

    [http://localhost:8080/users/websites](http://localhost:8080/users/websites)

2. O Nome, email e a empresa em que trabalha (em ordem alfabética).
	
	[http://localhost:8080/users/alphabeticOrder](http://localhost:8080/users/alphabeticOrder)

3. Mostrar todos os usuários que no endereço contem a palavra ```suite```
	 [http://localhost:8080/users/suites](http://localhost:8080/users/suites)


## Testes unitários

Entre na máquina com:

		vagrant ssh

Execute com o docker o comando:

    docker exec -it user-service npm run test



## Logs

Para acessar aos logs das requisições:
		
[http://localhost:9200/logapi_requests/_search?pretty=true&q=*:*](http://localhost:9200/logapi_requests/_search?pretty=true&q=*:*)

