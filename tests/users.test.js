import chai from 'chai';
import { listAllWebsites, listAllUsersAlphabeticOrder, listAllUsersWithSuite  }  from '../src/services/userListServices';
import chaiSorted from 'chai-sorted';

const assert = chai.assert;
const expect = chai.expect;

chai.use(chaiSorted);

const verifyTypeInArray = (array, type) => array.every((e) => typeof e == type);
const verifyIfKeyExists = (array, key) => array.every((e) => e.hasOwnProperty(key));
const verifyIfSuitesExist = (array) => array.every((e) => e.address.suite.indexOf('Suite') !== -1);

describe('TDD List Websites', async () => {
    it('Test: Should be a array', async () => {
        const listWebsitesResult = await listAllWebsites();
        assert.equal(Array.isArray(listWebsitesResult), true);
    });
    it('Test: Should contain only strings inside array', async () => {
        const listWebsitesResult = await listAllWebsites();
        assert.equal(verifyTypeInArray(listWebsitesResult, 'string'), true);
    });
});

describe('TDD List Alphabetic Order', () => {
    it('Test: Verify if name, email and company are in object', async () => {
        const listAlphabeticResult = await listAllUsersAlphabeticOrder();
        assert.equal(verifyIfKeyExists(listAlphabeticResult, 'name'), true);
        assert.equal(verifyIfKeyExists(listAlphabeticResult, 'email'), true);
        assert.equal(verifyIfKeyExists(listAlphabeticResult, 'company'), true);
    })
    it('Test: Should be all object in array', async () => {
        const listAlphabeticResult = await listAllUsersAlphabeticOrder();
        assert.equal(verifyTypeInArray(listAlphabeticResult, 'object'), true);
    });
    it('Test: Should be Alphabetic Sorted ', async () => {
        const listAlphabeticResult = await listAllUsersAlphabeticOrder();
        const onlyNamesToVerify = listAlphabeticResult.map((e) => e.name);
        expect(onlyNamesToVerify).to.be.sorted();
    });
});

describe('TDD List all websites with suite in suite', () => {
    it('Test: Should be all object in array', async () => {
        const listWithSuiteResult = await listAllUsersWithSuite();
        assert.equal(verifyTypeInArray(listWithSuiteResult, 'object'), true);
    });
    it('Test: Should be all address suite have "suite"', async () => {
        const listWithSuiteResult = await listAllUsersWithSuite();
        assert.equal(verifyIfSuitesExist(listWithSuiteResult), true);
    })
}); 